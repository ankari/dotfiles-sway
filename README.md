## Computer setup

1. Install Arch Linux following the wiki: https://wiki.archlinux.org/index.php/Installation_guide
2. Install `NetworkManager` or `wifi-menu` (`dialog` and
   `wpa_supplicant`)
3. Create user
4. Restart and connect to the internet

#Installing theme:

1. Install yay:
1.1. `git clone https://aur.archlinux.org/yay.git`
1.1. `cd yay`
1.1. `makepkg -si`
1. Install packages from packages.txt (yay -S - < packages.txt)
1. Install Vundle:
1.1. `mkdir -p ~/.vim/bundle && cd ~/.vim/bundle`
1.1. `git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim`
1.1. Start vim and run:
`:PluginInstall`
1. Install oh-my-zsh: `sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"`
1.1. Install terminal theme:` git clone https://github.com/bhilburn/powerlevel9k.git ~/.oh-my-zsh/custom/themes/powerlevel9k`
1.1. `git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions`
1.1 `git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting`
1. Symlink all files from the folder to the home directory
1. Enable networkmanager
1. Set up xdg dirsgit clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting

