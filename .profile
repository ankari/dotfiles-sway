export JAVA_HOME=/home/aron/.sdkman/candidates/java/current
export XCURSOR_PATH=/usr/share/icons/
export XCURSOR_THEME=Breeze
export DISPLAY=:0

# for wayland compatibility of java apps
export _JAVA_AWT_WM_NONREPARENTING=1

PATH=$PATH:$HOME/.yarn/bin:$HOME/.local/bin:~/work/dev/bin/:./scripts


## ANDOIRD DEV SETTINGS

export ANDROID_HOME=$HOME/Android/Sdk
export PATH=$PATH:$ANDROID_HOME/emulator
export PATH=$PATH:$ANDROID_HOME/tools
export PATH=$PATH:$ANDROID_HOME/tools/bin
export PATH=$PATH:$ANDROID_HOME/platform-tools

